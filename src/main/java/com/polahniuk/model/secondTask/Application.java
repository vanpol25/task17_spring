package com.polahniuk.model.secondTask;

import com.polahniuk.model.secondTask.bean.other.OtherBeanA;
import com.polahniuk.model.secondTask.bean.other.OtherBeanB;
import com.polahniuk.model.secondTask.bean.other.OtherBeanC;
import com.polahniuk.model.secondTask.config.SecondConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        final Logger log = LogManager.getLogger(Application.class);
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(SecondConfig.class);
        log.info(context.getBean(OtherBeanA.class));
        log.info(context.getBean(OtherBeanA.class));
        log.info(context.getBean(OtherBeanB.class));
        log.info(context.getBean(OtherBeanB.class));
        log.info(context.getBean(OtherBeanC.class));
        log.info(context.getBean(OtherBeanC.class));
    }
}
