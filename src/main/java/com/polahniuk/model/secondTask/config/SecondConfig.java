package com.polahniuk.model.secondTask.config;

import com.polahniuk.model.secondTask.bean.beans3.BeanD;
import com.polahniuk.model.secondTask.bean.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

@Configuration
@Import(FirstConfig.class)
@ComponentScan("com.polahniuk.model.secondTask.bean.beans2")
@ComponentScan( useDefaultFilters = false,
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BeanD.class, BeanF.class})
        })
public class SecondConfig {
}
