package com.polahniuk.model.secondTask.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.polahniuk.model.secondTask.bean.beans1")
@ComponentScan("com.polahniuk.model.secondTask.bean.other")
@ComponentScan("com.polahniuk.model.secondTask.bean.customBean")
public class FirstConfig {
}
