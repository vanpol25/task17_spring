package com.polahniuk.model.secondTask.config;

import com.polahniuk.model.secondTask.bean.beans2.CatAnimal;
import com.polahniuk.model.secondTask.bean.beans2.NarcissusFlower;
import com.polahniuk.model.secondTask.bean.beans2.RoseFlower;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class DevConfig {

    @Bean
    public CatAnimal catAnimal() {
        return new CatAnimal();
    }

    @Bean
    public NarcissusFlower narcissusFlower() {
        return new NarcissusFlower();
    }

    @Bean
    public RoseFlower roseFlower() {
        return new RoseFlower();
    }

}
