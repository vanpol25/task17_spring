package com.polahniuk.model.secondTask.bean.beans1;

import com.polahniuk.model.secondTask.bean.other.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanA {

    @Autowired
    @Qualifier(value = "otherBean")
    private OtherBeanA otherBeanA;

}
