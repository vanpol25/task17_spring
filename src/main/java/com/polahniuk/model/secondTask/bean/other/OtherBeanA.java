package com.polahniuk.model.secondTask.bean.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "otherBean")
@Scope("singleton")
public class OtherBeanA {
}
