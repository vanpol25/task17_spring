package com.polahniuk.model.secondTask.bean.beans1;

import com.polahniuk.model.secondTask.bean.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanC {

    private OtherBeanC otherBeanC;

    public BeanC() {
    }

    @Autowired
    public BeanC(OtherBeanC otherBeanC) {
        this.otherBeanC = otherBeanC;
    }

}
