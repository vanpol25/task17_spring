package com.polahniuk.model.secondTask.bean.customBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyBean {

    private List<Bean> beans;
    private Bean bean1;
    private Bean bean2;
    private Bean bean3;

    @Autowired
    public MyBean(List<Bean> beans, Bean bean1, @Qualifier(value = "beanE") Bean bean2, @Qualifier(value = "beanF") Bean bean3) {
        this.beans = beans;
        this.bean1 = bean1;
        this.bean2 = bean2;
        this.bean3 = bean3;
    }
}
