package com.polahniuk.model.secondTask.bean.beans3;

import com.polahniuk.model.secondTask.bean.customBean.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@Primary
public class BeanD implements Bean {
}
