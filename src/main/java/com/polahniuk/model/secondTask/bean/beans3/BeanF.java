package com.polahniuk.model.secondTask.bean.beans3;

import com.polahniuk.model.secondTask.bean.customBean.Bean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
@Qualifier(value = "beanF")
public class BeanF implements Bean {
}
