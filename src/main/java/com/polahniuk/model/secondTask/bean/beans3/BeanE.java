package com.polahniuk.model.secondTask.bean.beans3;

import com.polahniuk.model.secondTask.bean.customBean.Bean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Qualifier(value = "beanE")
public class BeanE implements Bean {
}
