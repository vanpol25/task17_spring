package com.polahniuk.model.firstTask.bean;

import com.polahniuk.model.firstTask.validator.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource("main.properties")
public class BeanB implements BeanValidator {

    private Logger log = LogManager.getLogger(BeanB.class);
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private String value;

    public void init() {
        log.info("init");
    }

    public void customInit() {
        log.info("customInit");
    }

    public void destroy() {
        log.info("destroy");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean validate() {
        return name != null && value.length() > 0;
    }
}