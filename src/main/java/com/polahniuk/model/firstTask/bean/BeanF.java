package com.polahniuk.model.firstTask.bean;

import com.polahniuk.model.firstTask.validator.BeanValidator;

public class BeanF implements BeanValidator {

    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean validate() {
        return name != null && value.length() > 0;
    }

}