package com.polahniuk.model.firstTask.bean;

import com.polahniuk.model.firstTask.validator.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class BeanPostProcessorImpl implements BeanPostProcessor {

    private static Logger log = LogManager.getLogger(BeanPostProcessorImpl.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator) {
            BeanValidator beanValidator = (BeanValidator) bean;
            if (beanValidator.validate()) {
                log.info(beanName + "  valid!");
            }
        }
        return bean;
    }

}
