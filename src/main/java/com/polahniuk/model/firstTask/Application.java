package com.polahniuk.model.firstTask;

import com.polahniuk.model.firstTask.config.SecondConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        final Logger log = LogManager.getLogger(Application.class);
        ApplicationContext ac = new AnnotationConfigApplicationContext(SecondConfig.class);
        for (String bean : ac.getBeanDefinitionNames()) {
            log.info(bean);
        }
    }
}
