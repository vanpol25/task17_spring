package com.polahniuk.model.firstTask.config;

import com.polahniuk.model.firstTask.bean.BeanA;
import com.polahniuk.model.firstTask.bean.BeanE;
import com.polahniuk.model.firstTask.bean.BeanF;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(FirstConfig.class)
public class SecondConfig {

    @Bean(name = "firstBeanE")
    public BeanE firstBeanE(@Qualifier(value = "firstBeanA") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean(name = "secondBeanE")
    public BeanE secondBeanE(@Qualifier(value = "secondBeanA") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean(name = "thirdBeanE")
    public BeanE thirdBeanE(@Qualifier(value = "thirdBeanA") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean(name = "beanF")
    @Lazy
    public BeanF beanF() {
        return new BeanF();
    }

}
