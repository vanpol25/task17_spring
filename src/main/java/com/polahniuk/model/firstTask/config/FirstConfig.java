package com.polahniuk.model.firstTask.config;

import com.polahniuk.model.firstTask.bean.BeanA;
import com.polahniuk.model.firstTask.bean.BeanB;
import com.polahniuk.model.firstTask.bean.BeanC;
import com.polahniuk.model.firstTask.bean.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class FirstConfig {

    @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD beanD() {
        return new BeanD();
    }

    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
    public BeanB beanB() {
        return new BeanB();
    }

    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
    public BeanC beanC() {
        return new BeanC();
    }

    @Bean(name = "firstBeanA")
    @DependsOn(value = {"beanB", "beanC"})
    public BeanA firstBeanA() {
        BeanA beanA = new BeanA();
        beanA.setName(beanB().getName() + beanC().getName());
        beanA.setValue(beanB().getValue() + beanC().getValue());
        return beanA;
    }

    @Bean(name = "secondBeanA")
    @DependsOn(value = {"beanB", "beanD"})
    public BeanA secondBeanA() {
        BeanA beanA = new BeanA();
        beanA.setName(beanB().getName() + beanD().getName());
        beanA.setValue(beanB().getValue() + beanD().getValue());
        return beanA;
    }

    @Bean(name = "thirdBeanA")
    @DependsOn(value = {"beanC", "beanD"})
    public BeanA thirdBeanA() {
        BeanA beanA = new BeanA();
        beanA.setName(beanC().getName() + beanD().getName());
        beanA.setValue(beanC().getValue() + beanD().getValue());
        return beanA;
    }

}
