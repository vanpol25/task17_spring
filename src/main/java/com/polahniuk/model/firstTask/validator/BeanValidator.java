package com.polahniuk.model.firstTask.validator;

public interface BeanValidator {

    boolean validate();

}
